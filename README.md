# CHCache

[![CI Status](http://img.shields.io/travis/yuanguowen/CHCache.svg?style=flat)](https://travis-ci.org/yuanguowen/CHCache)
[![Version](https://img.shields.io/cocoapods/v/CHCache.svg?style=flat)](http://cocoapods.org/pods/CHCache)
[![License](https://img.shields.io/cocoapods/l/CHCache.svg?style=flat)](http://cocoapods.org/pods/CHCache)
[![Platform](https://img.shields.io/cocoapods/p/CHCache.svg?style=flat)](http://cocoapods.org/pods/CHCache)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CHCache is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CHCache'
```

## Author

yuanguowen, ygwenx@gmail.com

## License

CHCache is available under the MIT license. See the LICENSE file for more info.
