//
//  CHViewController.m
//  CHCache
//
//  Created by yuanguowen on 11/04/2017.
//  Copyright (c) 2017 yuanguowen. All rights reserved.
//

#import "CHViewController.h"
//#import <CHCache/CHCache.h>
#import <CHCache.h>
@interface CHViewController ()
@property (weak, nonatomic) IBOutlet UITextView *textView;

@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@end

@implementation CHViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)save:(id)sender {
    [[CHCache cache] setObject:_textView.text forKey:@"test"];
    //    [[CHCache cache] setObject:_textView.text forKey:@"test" indate:1/60];
    [self showCacheContent];
}

- (IBAction)remove:(id)sender {
    [[CHCache cache] removeObjectForKey:@"test"];
    [self showCacheContent];
}

- (IBAction)change:(id)sender {
    [[CHCache cache] setObject:@"修改了test的缓存" forKey:@"test"];
    [self showCacheContent];
}

- (void)showCacheContent {
    _contentLabel.text = [[CHCache cache] objectForKey:@"test"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

