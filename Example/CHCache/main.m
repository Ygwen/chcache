//
//  main.m
//  CHCache
//
//  Created by yuanguowen on 11/04/2017.
//  Copyright (c) 2017 yuanguowen. All rights reserved.
//

@import UIKit;
#import "CHAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CHAppDelegate class]));
    }
}
