//
//  CHAppDelegate.h
//  CHCache
//
//  Created by yuanguowen on 11/04/2017.
//  Copyright (c) 2017 yuanguowen. All rights reserved.
//

@import UIKit;

@interface CHAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
