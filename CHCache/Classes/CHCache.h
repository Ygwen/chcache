//
//  CHCache.h
//  CHMerchant
//
//  Created by Ygwen on 2017/4/1.
//  Copyright © 2017年 CarHouse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CHCache : NSObject

/**
 生成一个独立cache

 @param name cahe名字
 @return cache实例
 */
- (instancetype)initWithName:(NSString *)name;

/**
 获取单例cache

 @return cache实例
 */
+ (instancetype)cache;

/**
 检查key对应的缓存是否过期  过期返回NO

 @param key 缓存的key
 @return 存在缓存且缓存有效返回YES，否则返回NO
 */
- (BOOL)isInDateForKey:(NSString *)key;

/**
 判断Key对应的缓存是否存在

 @param key 缓存的key
 @return 存在缓存返回YES，否则返回NO
 */
- (BOOL)containsObjectForKey:(NSString *)key;

/**
 根据键值进行存储 永久有效(可以主动删除)

 @param obj 需要缓存的数据
 @param key 缓存的key
 */
- (void)setObject:(id<NSCoding>)obj forKey:(NSString *)key;

/**
 根据键值进行存储 hour代表这个缓存有效期(可以主动删除)

 @param obj 需要缓存的数据
 @param key 缓存的key
 @param indateOfHour 有效期 /小时 0则代表永久有效
 */
- (void)setObject:(id<NSCoding>)obj forKey:(NSString *)key indate:(NSUInteger)indateOfHour;

/**
 根据key取出对应的数据 如果已经过了有效期则返回nil 同时会去删除过期的数据

 @param key 缓存的key
 */
- (id)objectForKey:(NSString *)key;

/**
 删除key对应的缓存

 @param key 缓存的key
 */
- (void)removeObjectForKey:(NSString *)key;

/**
 清除当前路径对应的缓存
 */
- (void)removeAllCache;

/**
 获取当前路径对应的缓存大小
 
 @return 缓存大小 单位为bytes
 */
- (NSInteger)getCacheSize;
@end
