//
//  CHCache.m
//  CHMerchant
//
//  Created by Ygwen on 2017/4/1.
//  Copyright © 2017年 CarHouse. All rights reserved.
//

#import "CHCache.h"
#import "YYCache.h"

@interface CHCache ()
/** <#注释#> */
@property(copy ,nonatomic) YYCache *dataCache;
/** <#注释#> */
@property(strong, nonatomic) NSCalendar *calenDar;
@end

@implementation CHCache

static NSString *const kCacheDateKey = @"kCacheDateKey";

+ (void)initialize {

}

- (NSCalendar *)calenDar {
    if (!_calenDar) {
        _calenDar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    }
    return _calenDar;
}

- (instancetype)initWithName:(NSString *)name {
    if (self = [super init]) {
        _dataCache = [YYCache cacheWithName:name];
        _dataCache.diskCache.freeDiskSpaceLimit = 200 * 1024 * 1024;
    }
    return self;
}

+ (instancetype)cache {
    static CHCache *cacheObj = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cacheObj = [[self alloc] initWithName:@"CHCache"];
    });
    return cacheObj;
}

- (BOOL)isInDateForKey:(NSString *)key {
    BOOL result = YES;
    if ([self containsObjectForKey:key]) {
        NSDictionary *cacheDic = [self objectForKey:key];
        NSString *dateString = [cacheDic objectForKey:kCacheDateKey];
        if (![dateString isEqualToString:@"ALWAY"]) {
            NSDate *cacheDate = [NSDate dateWithTimeIntervalSince1970:[dateString doubleValue]];
            if ([[NSDate date] compare:cacheDate] == NSOrderedDescending) {
                result = NO;
            }
        }
    }
    else { result = NO; }
    return result;
}

- (BOOL)containsObjectForKey:(NSString *)key {
    return [_dataCache containsObjectForKey:key];
}

- (void)setObject:(id<NSCoding>)obj forKey:(NSString *)key {
    [self setObject:obj forKey:key indate:0];
}

- (void)setObject:(id<NSCoding>)obj forKey:(NSString *)key indate:(NSUInteger)indateOfHour {
    if (!obj) { return; }
    NSDate *date = indateOfHour>0?[[NSDate date] dateByAddingTimeInterval:(indateOfHour * 60 * 60)]:[NSDate date];
    NSString *dateString = indateOfHour>0?[NSString stringWithFormat:@"%ld",(long)[date timeIntervalSince1970]]:@"ALWAY";
    NSMutableDictionary *cacheDic = @{}.mutableCopy;
    [cacheDic setValue:obj forKey:key];
    [cacheDic setValue:dateString forKey:kCacheDateKey];
    [_dataCache setObject:cacheDic forKey:key withBlock:nil];
}

- (id)objectForKey:(NSString *)key {
    id cacheDic = [_dataCache objectForKey:key];
    id result = [cacheDic objectForKey:key];
    return result;
}

- (void)removeObjectForKey:(NSString *)key {
    [_dataCache removeObjectForKey:key];
}

- (void)removeAllCache {
    [_dataCache removeAllObjects];
}

- (NSInteger)getCacheSize {
    return [_dataCache.diskCache totalCost];
}

@end
