
Pod::Spec.new do |s|
  s.name             = 'CHCache'
  s.version          = '1.0'
  s.summary          = 'CHCache'
  s.description      = '此处填上组件的描述'
  s.homepage         = 'http://192.168.1.221/carhouse-ios/CHCache'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'yuanguowen' => 'ygwenx@gmail.com' }
  s.source           = { :git => 'http://192.168.1.221/carhouse-ios/CHCache.git', :tag => s.version.to_s }
  s.ios.deployment_target = '7.0'
  s.requires_arc = true
  if ENV['IS_SOURCE'] || ENV['CHCache_SOURCE']
    s.source_files = 'CHCache/Classes/**/*'
  else
    s.vendored_frameworks = 'CHCache/Products/*.framework'
  end

  # s.resource_bundles = {
  #   'CHCache' => ['CHCache/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'YYCache', '1.0.4'
end
